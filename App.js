import React from 'react';
import { StyleSheet, Text, View, StatusBar , route} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import AppNavigator from './navigation/AppNavigator';
// import {Font} from 'expo';

export default class App extends React.Component {

	render() {
		return (
			<AppNavigator />
		);
	}
}

const styles = StyleSheet.create({
	container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
    alignItems: 'center',
    justifyContent: 'center',
	},
});
