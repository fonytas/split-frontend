
import axios from "axios";

let isRefreshing = false;
let refreshSubscribers = [];

// const base_URL = "10.226.160.138"
// const base_URL = "192.168.0.101" // dorm
// const base_URL = "172.20.10.8"
const base_URL = "10.5.50.123"
const base_port ="8080"

const API_URL = "http://"+base_URL+":"+base_port;
// const API_URL = "https://b653942d.ngrok.io"
axios.defaults.headers.common["Accept"] = "application/json";
// change ip itself
// 5000
if (__DEV__) {
    axios.defaults.headers.common["Origin"] =  API_URL
}

const API = axios.create({
    baseURL: API_URL
});


export default API;


