

import React from 'react';
import { View, Text ,StyleSheet,  TouchableOpacity,  Image} from "react-native";
import API from "../constants/API";
import AddGroupIcon from '../assets/Icons/addGroup.png'
import Loading from './Loading';

export default class GroupList extends React.Component {

    constructor(props) {
        super()
        this.state = {
            members: [],
            authKey: "",
            groupName: "",
        }
    }

    handleAddGroup = () => {
        this.props.navigation.navigate('CreateGroup', {authKey: this.state.authKey, getGroup: this.props.getGroup});
    }

    handleClickGroup = (groupName) => {
        const payload = {
            members: this.state.members,
        };

        var headers = {
            'API_KEY': this.state.authKey
        }

        API.post(`/create-group?groupName=${this.state.groupName}`, payload, {headers: headers})
        .then((response) => {
            console.log("OK!")
        })
        .catch((error) => {
            console.log(error)
        })
    }

    render () {

        const {groups, isLoadingGroup} = this.props
        if (isLoadingGroup) {
            return <Loading />
        }
        else {
            return (
                <View>
                    <View style={{flexDirection:'row',justifyContent: 'space-between',borderBottomWidth: 1, borderColor:'#E9EEF2'}}>
                            <Text style={styles.headerText}>GROUPS</Text> 
                            <TouchableOpacity onPress={() => this.handleAddGroup()}>
                                <Image source={AddGroupIcon} resizeMode={'contain'}  style={{flex:1, tintColor: '#A076E8', height:'50%', width:45, margin:8}} />
                                
                            </TouchableOpacity>
                    </View>

                    <View>
                        {
                        groups.map((item, index) => (
                            <TouchableOpacity
                                
                                style = {styles.container}
                            >
                                <Text style={styles.friendListText}
                                    onPress={() => this.props.handleClickGroup(item)}>
                                    {item}
                                </Text>
                            </TouchableOpacity>
                        ))
                        }
                    </View>
                </View>
            )
        }

    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#fff',
        borderColor: '#E9EEF2',
        borderBottomWidth: 1,
        height: 45,
        justifyContent: "center",    
    },
    friendListText: {
        color: 'black',
        fontSize: 15,
        fontWeight: '200',
        paddingLeft: 15,   
    },
    headerText: {
        fontWeight: '900',
        padding: 10,
    }
});
