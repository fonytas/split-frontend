import React from 'react';
import { View, Text ,StyleSheet, TextInput, Keyboard, TouchableWithoutFeedback} from "react-native";

const DismissKeyboard = ({children})=>(
    <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
export default class SplitExactAmount extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            
        }
    }

    

    render () {
        const {name, createSplitUnquallySummary} = this.props
        return (
            <DismissKeyboard>
                <View>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}>

                        <View style={{flex: 2, paddingTop: 10, paddingLeft: 10}}>
                            <Text style={styles.textStyle} > {name} </Text>
                        </View>

                        <View style={{flex: 1}}>
                            <TextInput 
                                style={styles.textInputStyle}
                                placeholder="0.00"
                                onChangeText={(amount) => createSplitUnquallySummary(name, amount)}
                                keyboardType='numeric'
                                maxLength={8}
                                autoCorrect={false} 
                            />
                        </View>
                    </View>
                </View>
            </DismissKeyboard>
        )
    }
}

const styles = StyleSheet.create({
    inputBox: {
        height: 40, 
        borderColor: 'gray',
        borderBottomWidth: 1,
    },

    textStyle: {
        fontSize: 17,
        fontWeight: '500',
        alignItems: 'center',
    },

    textInputStyle: {
        borderBottomWidth: 1,
        borderColor: 'black',
        height: 30,
        width: '90%',
        paddingLeft: 20,
        alignItems: 'flex-end',
        fontSize: 20,
        fontWeight: '200'
    },
    
});
