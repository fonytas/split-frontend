import React from 'react';
import { View, Text ,StyleSheet, Alert, Image, TouchableOpacity, AsyncStorage} from "react-native";
import API from "../constants/API";
import AddFriendIcon from '../assets/Icons/AddFriend.png'
import Dialog from "react-native-dialog";

export default class AddFriend extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            dialogVisible: false,
            input: "",
            authKey: ""
        }
    }

    componentDidMount () {
        this._retrieveData()
    }

    render () {
        return (
            <View>
                
                <View style={{flexDirection:'row',justifyContent: 'space-between'}}>
                    <Text style={styles.headerText}>FRIENDS </Text>
                    <TouchableOpacity onPress={() => this.handleAddFriend()}>
                        <Image source={AddFriendIcon} resizeMode={'contain'}  style={{flex:1, tintColor: '#A076E8', height:'60%', width:55, margin:5}} />  
                    </TouchableOpacity>
                </View>

                <View>
                    <Dialog.Container visible={this.state.dialogVisible}>
                    <Dialog.Title>Add Friend</Dialog.Title>
                        <Dialog.Input  onChangeText={(input) => this.setState({input})}></Dialog.Input>
                    
                    <Dialog.Button label="Cancel" onPress={this.handleCancel} />
                    <Dialog.Button label="Add" onPress={this.handleAdd} />
                    </Dialog.Container>
                </View>

            </View>
        )
    }

    handleAddFriend = () => {
        this.showDialog()
    }
    showDialog = () => {
        this.setState({ dialogVisible: true });
    };

    handleCancel = () => {
    this.setState({ dialogVisible: false, input: "" });
    };
    
    handleAdd = async() => {
        const value = await AsyncStorage.getItem('API_KEY');
        var headers = {
            'API_KEY': value
        }
    
        const payload = {
        };

        if (this.state.input.length >= 5) {
            API.post(`/add-friend?friendName=${this.state.input}`,  payload,{headers: headers})
                .then((response) => {
                    this.setState({ dialogVisible: false });
                    this.props.getFriends()
                })
                .catch((error) => {
                    // Alert.alert(error.response)
                    // console.log(">>>",error.response.data == "")
                    if (error.response.data == "") {
                        Alert.alert("Username does not exist")
                    }
                    else {Alert.alert(error.response.data)}
                })  
        }
        else {
            Alert.alert("The username should be more than 5 characters")
        }
        
    };

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('API_KEY');
            if (value !== null) {
                this.setState({authKey: value})  
            }
            
        } catch (error) {
            console.log(error)
        }
    };
}

const styles = StyleSheet.create({
    headerText: {
        fontWeight: '900',
        padding: 10,
        flex:6
    },
    container: {
        alignItems: "center", 
        justifyContent: "center",
        backgroundColor: "white",
        borderColor: 'white',
        borderRadius: 25,
        borderWidth: 1,
        borderStyle: 'solid',
        
        paddingTop: 80,
        paddingBottom: 80,
        paddingLeft: 30,
        paddingRight: 30,

        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.1,
        elevation: 1

    }

});
