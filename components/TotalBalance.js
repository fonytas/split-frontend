import React from 'react';
import { View, Text ,StyleSheet} from "react-native";

export default class TotalBalance extends React.Component {

    constructor(props) {
        super()
        this.state = {  
            authKey: ""
        }
    }

    
    
    render () {
        const { totalBalance, youOwe, youAreOwed, hasDebt } = this.props

        if (hasDebt) {
            text = (<View style={ styles.BoxRed }> 
                        <Text style={styles.totalBalanceText}> - THB {Math.abs(totalBalance)} </Text>
                    </View>)
        } else {
            text = (<View style={ styles.BoxGreen }> 
                <Text style={styles.totalBalanceText}> + THB {totalBalance} </Text>
            </View>)
        }
        

        return (
            <View style={{ 
                    flex: 1, 
                    flexDirection: 'row', 
                    justifyContent: 'space-around',
                    paddingTop: 16
            }}>
                <View style= {styles.totalBalanceBox}>
                    <View style={{ paddingBottom: 3}}>
                        <Text style={styles.headerText}> TOTAL BALANCE </Text>
                    </View>
                    {text}
                </View>

                
                <View style={ styles.oweBox } > 
                    <View style={{ paddingBottom: 3}}>
                        <Text style={styles.headerText}> YOU OWE </Text>
                    </View>
                    <View style={ styles.youOweBox }>
                        <Text style={styles.OweText} > THB {youOwe} </Text>
                    </View>
                    
                </View>

                <View style={ styles.oweBox }>
                    <View style={{ paddingBottom: 3}}>
                        <Text style={styles.headerText}> YOU ARE OWED </Text>
                    </View>
                    <View style={ styles.youAreOwedBox }>
                        <Text style={styles.OweText}> THB {youAreOwed} </Text>
                    </View>
                </View>
                
            </View>
            
        )
    }
    
}

const styles = StyleSheet.create({
    totalBalanceBox: {
        width: '43%', 
        height: 54, 
    },
    
    BoxRed: {
        width: '100%', 
        height: '100%', 
        backgroundColor: '#ee6369',

        borderWidth: 1,
        borderColor: '#ee6369',
        borderRadius: 10,

        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.2,
        elevation: 1,

        alignItems: "center", 
        justifyContent: "center",
    },
    BoxGreen: {
        width: '100%', 
        height: '100%', 
        backgroundColor: '#53b77f',

        borderWidth: 1,
        borderColor: '#53b77f',
        borderRadius: 10,

        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.2,
        elevation: 1,

        alignItems: "center", 
        justifyContent: "center",
    

    },
    oweBox: {
        width: '22%',
        height: 54,
    },
    youOweBox: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ee6369',

        borderWidth: 1,
        borderColor: '#ee6369',
        borderRadius: 10,
        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.2,
        elevation: 1,

        alignItems: "center", 
        justifyContent: "center",

    },
    youAreOwedBox: {
        width: '100%',
        height: '100%',
        backgroundColor: '#53b77f',

        borderWidth: 1,
        borderColor: '#53b77f',
        borderRadius: 10,
        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.2,
        elevation: 1,

        alignItems: "center", 
        justifyContent: "center",

    },

    totalBalanceText: {
        
        color: '#fcfcfc',
        fontSize: 21,
        fontWeight: '200',
    },
    OweText: {
        color: '#fcfcfc',
        fontSize: 12.5,
        fontWeight: '200',
    },

    headerText: {
        fontWeight: '900',
        fontSize: 10,
        color: '#a2a2a2'
    }

});
