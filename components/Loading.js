import React from 'react';
import { ActivityIndicator,  StyleSheet,View} from "react-native";


export default class Loading extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render () {
        
        return (
            <View style={styles.container}>
                <ActivityIndicator size="small" color="#A076E8" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
});
