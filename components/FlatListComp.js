import React from 'react';
import { View, Text ,StyleSheet, FlatList, TouchableOpacity} from "react-native";
import AddFriend from './AddFriend';

export default class FlatListComp extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    _keyExtractor = (item, index) => item.toString();

    renderItem = ({item}) => (
        <View>
            <TouchableOpacity onPress={() => this.props.handleClick(item)}  style = {styles.container}>
                <Text style={styles.ListText} >
                    {item} 
                </Text>
            </TouchableOpacity>
        </View>
    )

    renderHeader = () => {
        if (this.props.header == "FRIENDS") {
            return (<AddFriend  getFriends={this.props.getFriends}/>)
        }
        else {
            return (
            <View style={{borderBottomWidth: 1, borderColor:'#E9EEF2'}}> 
                <Text style={styles.headerText}>{this.props.header}</Text>
            </View>
            )
        }  
    }
    
    render () {
        const {data} = this.props;
        return (
            <View>
                <FlatList
                    data={data}
                    extraData={this.state}
                    keyExtractor={this._keyExtractor}
                    renderItem={this.renderItem}
                    ListHeaderComponent={this.renderHeader}      
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({

    ListText: {
        color: 'black',
        fontSize: 15,
        fontWeight: '200',
        paddingLeft: 15,
    },
    container: {
        backgroundColor: '#fff',
        borderColor: '#E9EEF2',
        borderBottomWidth: 1, 
        height: 45,
        justifyContent: "center",
    },
    headerText: {
        fontWeight: '900',
        padding: 15,
        fontSize: 15   
    }
});
