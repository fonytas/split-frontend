import React from 'react';
import { View, Text ,StyleSheet, FlatList} from "react-native";
import { Avatar } from "react-native-elements";


export default class Events extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    _keyExtractor = (item) => item;

    _renderItem = ({item}) => {
        return (<View style={{paddingRight: 5}}>
                    <Avatar
                        size="small"
                        rounded
                        title={item.substring(0,2)}
                        activeOpacity={0.7}
                        overlayContainerStyle={{backgroundColor: '#a076e8'}}
                    />
                </View>)
    }

    render () {
        const { eventName, createdBy, date, totalBalance, members  } = this.props 
        
        return (
            <View>
                <View style={styles.container}>
                        <View style={{paddingBottom: 5, alignItems: 'center',}}><Text style={styles.eventName}>{eventName}</Text></View>
                        <View style={{paddingBottom: 5, alignItems: 'center',}}><Text style={styles.text}>{date}</Text></View>
                        <View style={{paddingBottom: 5, alignItems: 'center',}}><Text style={styles.totalBalance}>THB {totalBalance}</Text></View>
                        <View
                        style={{
                            borderBottomColor: '#F0ECEC',
                            borderBottomWidth: 1,
                            padding: 10
                        }}
                        />
                        <View style={{paddingTop: 15, flexDirection: 'row'}}>
                            
                            <FlatList
                                data={members}
                                keyExtractor={this._keyExtractor}
                                renderItem={this._renderItem}
                                horizontal={true}
                            />
                        </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        borderWidth: 1,
        borderColor: '#a076e8',
        padding: 10,
        backgroundColor: 'white', 
        flex: 1    
    },
    eventName: {
        fontWeight: '800'
    },
    text: {
        color: '#888484',
        fontWeight: '200'
    },
    totalBalance: {
        fontWeight: '300',
        color: '#a076e8',
        fontSize: 18,
    }
});
