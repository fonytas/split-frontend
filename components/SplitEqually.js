import React from 'react';
import { View, Text ,StyleSheet} from "react-native";

export default class SplitEqually extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            payAmount: 0
        }
    }

    componentDidMount () {
        const {totalBalance, selectedMembers} = this.props
        this.setState({payAmount: (totalBalance/selectedMembers.length).toFixed(2)})
    }

    render () {
        const {payAmount} = this.state
        const {totalBalance} = this.props
    

        return (
            <View style={{backgroundColor: '#F3F2FB', flex: 1, alignItems: 'center'}}>
                <View style={{paddingTop: 25}}>
                    <Text style={styles.textBalance}>Total Balance: {totalBalance} </Text>
                    <Text style={styles.textBalance}>THB {payAmount} /person </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    textBalance: {
        fontSize: 30,
        fontWeight: '200'
    }

});
