import React from 'react';
import { View, Text ,StyleSheet, FlatList, ScrollView} from "react-native";
import SplitExactAmount from './SplitExactAmount';

export default class SplitUnequally extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            totalBalance: this.props.totalBalance,
            selectedMembers: this.props.selectedMembers,
            paidAmount: 0.00,
            splitUnquallySummary: {},
            remainingAmount: 0
        }
    }
    componentDidMount () {
        const { selectedMembers } = this.state
        let nameList = {}
        const arrayLength = selectedMembers.length;

        for (let i = 0; i < arrayLength; i++) {
            let name = selectedMembers[i]
            nameList[name] = parseFloat(0)
        }
        this.setState({splitUnquallySummary: nameList})
    }
    
    createSplitUnquallySummary = (name, amount) => {

        const {splitUnquallySummary, totalBalance} = this.state
        const {setSplitUnquallySummary} = this.props

        let old = splitUnquallySummary;
        let remain = 0.00;

        if (isNaN(old[name]) || isNaN(amount)) {
            old[name] = 0.00
        }
        else {
            old[name] = parseFloat(amount);
        }
        
        for (let each in old) {
            if (isNaN(old[each])) {
                old[each] = 0.00
            }
            remain += (old[each])
        }

        remain = remain.toFixed(2)
        
        if (isNaN(remain)) {
            this.setState({remainingAmount: 0.00})
        } else {
            this.setState({remainingAmount: remain})
        }

        this.setState({splitUnquallySummary: old})
        setSplitUnquallySummary(splitUnquallySummary)
        
        let bal = parseFloat((totalBalance - remain).toFixed(2))
        this.props.onIsDone(bal)
    }

    
    _onRemainingBalanceUpdate = () => {
        const {totalBalance, remainingAmount} = this.state
        let bal = parseFloat((totalBalance - remainingAmount).toFixed(2))
        return bal
    }

    _keyExtractor = (item) => item;

    _renderItem = ({item}) => (
        <View style={{paddingTop: 20}}>
            <SplitExactAmount 
                createSplitUnquallySummary={this.createSplitUnquallySummary}
                name={item}
                remainingAmount={this.props.remainingAmount}
            />
        </View>
    );
    

    render () {
        const {totalBalance, selectedMembers, remainingAmount} = this.state;

        return (
            <ScrollView style={{backgroundColor: '#F3F2FB', flex: 1}}>
                <View style={{alignItems: 'center', paddingTop: 20}}>            
                    <Text style={styles.textBalance}>Total: {remainingAmount} of {totalBalance}</Text>
                    <Text style={styles.remainingBalance}> {this._onRemainingBalanceUpdate()} Left</Text>
                </View>
                <View>
                    <FlatList
                        data={selectedMembers}
                        extraData={this.state}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                    />
                </View>
                
                
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({

    textBalance: {
        fontSize: 30,
        fontWeight: '500'
    },
    remainingBalance: {
        fontSize: 15,
        fontWeight: '200'
    }

});
