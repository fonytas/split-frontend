import React from 'react';
import { TextInput,StyleSheet,View } from 'react-native';


export default class TextInputComp extends React.Component {


    render () {
        return (
            <View style={styles.inputBorder}> 
                <TextInput
                    {...this.props}
                    style={styles.input}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    input: {
        textAlign: 'center',
        fontSize: 15,
        paddingTop: 13,
    },
    
    inputBorder: {
        height: 50,
        width: 235,
        backgroundColor: '#FFF',
        borderColor: '#EAE7E7',
        borderRadius: 25,
        borderWidth: 1,
        borderStyle: 'solid',
    }
});