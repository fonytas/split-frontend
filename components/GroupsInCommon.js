import React from 'react';
import { View, Text ,StyleSheet, TouchableOpacity, AsyncStorage} from "react-native";
import API from "../constants/API";


export default class GroupsInCommon extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            groupsInCommon: [],
            authKey: ""
        }
    }

    componentDidMount () {
        this.getGroupInCommon()
    }

    getGroupInCommon = async() => {
        const { friendName} = this.props
        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})
        var headers = {
            'API_KEY': value
        }
        API.get(`/get-group-in-common?friendName=${friendName}`, {headers: headers})
        .then((response) => {
            let data = response.data
            this.setState({groupsInCommon: data})
        })
        .catch((error) => {
            console.log(error)
        })
    }




    render () {
        return (
            <View>
                <Text style={styles.headerText}>GROUPS IN COMMON</Text>
                <View>
                    {
                    this.state.groupsInCommon.map((item, index) => (
                        // <TouchableOpacity
                        //     key = {item}
                        //     style = {styles.container}
                        // >

                        <View style = {styles.container} key={item}>
                            <Text style={styles.groupListText}>
                                {item}
                            </Text>
                        </View>
                        // </TouchableOpacity>
                    ))
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    headerText: {
        fontWeight: '900',
        padding: 10,
    },
    container: {
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#E9EEF2', 
        height: 45,
        justifyContent: "center", 
    },
    groupListText: {
        color: 'black',
        fontSize: 15,
        fontWeight: '200',
        paddingLeft: 15,
    },


});
