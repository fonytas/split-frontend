

import React from 'react';
import { View, Text ,StyleSheet, TouchableOpacity} from "react-native";
import AddFriend from './AddFriend';
import Loading from './Loading';


export default class FriendList extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            authKey: "",
        }
    }

    render () {
        const {names, isLoadingFriend}= this.props
        if (isLoadingFriend) {
            return <Loading />
        }
        else {
            return (
                    <View>
                        <AddFriend getFriends={this.props.getFriends}/>
                        {
                        names.map((item, index) => (
                            <TouchableOpacity
                                key = {item}
                                style = {styles.container}

                            >
                                <Text style={styles.friendListText}
                                    onPress={() => this.handleClickFriend(item)} >
                                    {item}
                                </Text>
                            </TouchableOpacity>
                        ))
                        }
                    </View>
            )
        }
    }

    handleClickFriend = (friendName) => {
        this.props.navigation.navigate('FriendInfo', {name : friendName });
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#E9EEF2',
        height: 45,
        justifyContent: "center",
    },
    friendListText: {
        color: 'black',
        fontSize: 15,
        fontWeight: '200',
        paddingLeft: 15,
    },
    headerText: {
        fontWeight: '900',
        padding: 10,
        flex:5
    }
});
