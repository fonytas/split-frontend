import React from 'react';
import { Image , Button} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import ProfileScreen from '../screens/ProfileScreen'
import FriendScreen from '../screens/FriendsScreen'
import AddBillScreen01 from '../screens/AddBillScreen01'
import FriendInfoScreen from '../screens/FriendInfoScreen';
import CreateGroupScreen from '../screens/CreateGroupScreen'

import ProfileIcon from '../assets/Icons/Profile.png'
import FriendIcon from '../assets/Icons/Friends.png'
import AddBillIcon from '../assets/Icons/AddBill.png'
import AddBillScreen02 from '../screens/AddBillScreen02';
import AddBillMain from '../screens/AddBillMainScreen';
import SplitOptionsScreen from '../screens/SplitOptionsScreen';
import PaidByScreen from '../screens/PaidByScreen';
import GroupInfoScreen from '../screens/GroupInfoScreen';

const FriendsStack = createStackNavigator({
    Friends: {
        screen: FriendScreen,
        navigationOptions: {
            title: 'Friends',
            headerStyle: {
                backgroundColor: '#A076E8',
            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
        }
    },
    FriendInfo: {
        screen: FriendInfoScreen,
        // navigationOptions: {
        //     title: 'Friends',
        //     headerStyle: {
        //         backgroundColor: '#A076E8',
        //     },
        //     headerTintColor: '#FCFCFC',
        //     headerTitleStyle: {
        //     fontWeight: 'bold',
        //     },
        // }
    },
    CreateGroup: {
        screen: CreateGroupScreen,
        
    },
    GroupInfo: {
        screen: GroupInfoScreen
    },
    
})


const AddBillStack = createStackNavigator({
    AddBill01: {
        screen: AddBillScreen01,
        navigationOptions: {        
            title: 'Select a group',
            headerStyle: {
                backgroundColor: '#A076E8',

            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
        }
    },


    AddBill02: {
        screen: AddBillScreen02,
        navigationOptions: {        
            title: 'Select friend(s)',
            headerStyle: {
                backgroundColor: '#A076E8',

            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
            
        }
    },


    AddBillMain: {
        screen: AddBillMain,
        navigationOptions: {        
            title: 'Add a bill',
            headerStyle: {
                backgroundColor: '#A076E8',

            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
            
        }

    },

    SplitOptions: {
        screen: SplitOptionsScreen,
        navigationOptions: {        
            title: 'Split Options',
            headerStyle: {
                backgroundColor: '#A076E8',

            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
            
        }
    },

    PaidBy: {
        screen: PaidByScreen,
        navigationOptions: {        
            title: 'Paid by',
            headerStyle: {
                backgroundColor: '#A076E8',

            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
            
        }
    }


})

const ProfileStack = createStackNavigator({
    Profile: {
        screen: ProfileScreen,
        navigationOptions: {
            title: 'Profile',
            headerStyle: {
                backgroundColor: '#A076E8',

            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
        }
    }
})

const StacksInTabs =  createBottomTabNavigator({
    Friends: {
        screen: FriendsStack,
        navigationOptions:{
            
            tabBarLabel: "Friends",
            tabBarIcon: ({tintColor, focused}) => (
                <Image source={FriendIcon} resizeMode={'contain'} style={{flex:1, tintColor: tintColor, margin: 2, width:'100%', height: '100%'}} />
            ),
            
        },
        
    },
    AddBill: {
        screen: AddBillStack,
        navigationOptions: () => ({
            tabBarLabel: "Add a Bill",
            tabBarIcon: ({tintColor, focused}) => (
                <Image source={AddBillIcon} resizeMode={'contain'} style={{flex:1, tintColor: tintColor, margin: 2, width:'100%', height: '100%'}} />
            )
            // https://github.com/alex-melnyk/Tabber/blob/master/src/navigation/BaseNavigator.js
            // https://itnext.io/react-native-tab-bar-is-customizable-c3c37dcf711f
        })
    },
    Profile: {
        screen: ProfileStack,
        navigationOptions: () => ({
            tabBarLabel: "My Profile",
            tabBarIcon: ({tintColor, focused}) => (
                <Image source={ProfileIcon} resizeMode={'contain'} style={{flex:1, tintColor: tintColor, margin: 2, width:'100%', height: '100%'}} />
            )
            
        })
    }
}, {
    
    tabBarOptions: {
        inactiveTintColor: 'gray',
        activeTintColor: '#A076E8',

        


    },
    initialRouteName: 'Friends', //temporary
    
})

export default StacksInTabs;