import React from 'react';
import { createAppContainer, createStackNavigator, StackNavigator, createSwitchNavigator } from 'react-navigation';
import SignUpScreen from '../screens/SignUpScreen';
import SignInScreen from '../screens/SignInScreen';
import FriendsScreen from '../screens/FriendsScreen';
import MainTabNavigator from './MainTabNavigator';



const AppStack = createStackNavigator({
    Main: {
        screen: MainTabNavigator,
        navigationOptions: {
            header: null
        }
    }
})


const AuthStack = createStackNavigator({
    SignUp: {
        screen: SignUpScreen,
        navigationOptions: {
            header: null
        }
    },
    SignIn: {
        screen: SignInScreen,
        navigationOptions: {
            header: null
        }
    },

},{
    initialRouteName: 'SignIn',

});



const NavAppNavigator = createStackNavigator ({
    Main: {
        screen: AppStack,
        navigationOptions: {
            header: null,
            gesturesEnabled: false,
        }
    },
    Auth: {
        screen: AuthStack,
        navigationOptions: {
            header: null,
            gesturesEnabled: false,
        }
    }

}, {
    
    initialRouteName: 'Main',

})

const AppNavigator = createAppContainer(NavAppNavigator)
export default AppNavigator;


