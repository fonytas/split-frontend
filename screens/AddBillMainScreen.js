import React from 'react';
import { View, Text ,StyleSheet,  TextInput, Button, Image, Alert, TouchableOpacity, Keyboard, TouchableWithoutFeedback} from "react-native";
import { StackActions, NavigationActions } from 'react-navigation';
import API from "../constants/API";
import BillIcon from '../assets/Icons/bill.png'

const DismissKeyboard = ({children})=>(
    <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'AddBill01' })],
});


export default class AddBillMainScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            eventName: "",
            totalBalance: 0,
            paidBy: "",
            selectedMembers: [],
            splitOption: "Equally",
            index: 0,
            splitUnquallySummary: {},
            remainingAmount: 0.00,
        }
    }


    
    componentDidMount () {
        const { selectedMembers} = this.props.navigation.state.params
        this.setState({selectedMembers: selectedMembers})
        this.props.navigation.setParams({handlePress: () => this._onPressButton()});
    }

    setSplitUnquallySummary = (summary) => {
        this.setState({splitUnquallySummary: summary})
    }

    _onSaveSummary = (summary) => {
        const { groupName, authKey} = this.props.navigation.state.params
        const { eventName, totalBalance, paidBy, selectedMembers} = this.state

        if (eventName == "" || totalBalance == 0 || paidBy == "" || 
        selectedMembers == [] || groupName == "" || summary == {}) {
            
            Alert.alert("Please complete all the fields.")
        }

        else {
            const payload = {
                eventName: eventName,
                totalBalance: parseInt(totalBalance),
                paidBy: paidBy[0],
                members: selectedMembers,
                groupName: groupName,
                summary: summary
            };

            let headers = {
                'API_KEY': authKey
            }    
            
    
            API.post(`/save-bill`, payload, {headers: headers})
            .then((response) => {
                console.log("OK!")
                this.props.navigation.dispatch(resetAction);
                this.props.navigation.navigate('Friends')
            })
            .catch((error) => {
                console.log(error)
            })
        }
    }

    createPaymentSummaryEqually = () => {
        const { totalBalance,selectedMembers } = this.state

        count = 1
        memberLength = selectedMembers.length
        splitAmount = (totalBalance/memberLength).toFixed(2)
        track = 0
        summary = {}
        selectedMembers.map( (name) => {

            if (count == memberLength) {
                summary[name] =  totalBalance - track
            }else {
                summary[name] = splitAmount
            }   
            count++  
            track += parseFloat(splitAmount)
        })
        return summary
    }


    _onPressButton = () => {
        const { splitUnquallySummary } = this.state
        summary = this.createPaymentSummaryEqually()

        if (this.state.splitOption == "Equally" ) {
            this._onSaveSummary(summary)
        }
        else {
            this._onSaveSummary(splitUnquallySummary)
        }
    }

    static navigationOptions = ({navigation}) => {

        const {params} = navigation.state;
    
        return {
            headerRight: (
                <Button
                    title = "Save"
                    onPress = {() => params.handlePress()}
                    color="#fff"
                />
            ),

        }
    }

    onSetPaidBy = (name) => {
        this.setState({paidBy : name})
    }

    _handleIndexChange = index => {
        if (index == 1) {
            this.setState({splitOption: "Exact Amount"})
        } else{
            this.setState({splitOption: "Equally"})
        }
        this.setState({ index: index });
    }

    _onChangeSplitBy = () => {
        const { totalBalance, selectedMembers, index} = this.state

        if (totalBalance == 0){
            Alert.alert("Please add the total balance amount")
        }
        else {
            this.props.navigation.navigate('SplitOptions', 
                                        {totalBalance: totalBalance,
                                        selectedMembers: selectedMembers,
                                        handleIndexChange: this._handleIndexChange,
                                        index: index,
                                        remainingAmount: this.state.remainingAmount,
                                        splitUnquallySummary: this.state.splitUnquallySummary,
                                        setSplitUnquallySummary: this.setSplitUnquallySummary
                                    })
        }
    }


    render () {
        
        return (
            <DismissKeyboard>
                <View style={{flex: 1, flexDirection: 'column',  backgroundColor: '#F3F2FB'}}>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around',backgroundColor: 'white', paddingTop: 15}}>
                            <View style={{paddingHorizontal:15, paddingTop: '15%', }}>
                                <Image source={BillIcon} resizeMode={'contain'}  style={{ tintColor: '#A076E8', height:55, width:45}} />
                            </View>

                            <View style={{flex: 2}}>
                                <Text style={styles.headerText}>Event Name </Text>

                                <TextInput 
                                    style={styles.eventNameStyle}
                                    placeholder="Event Name"
                                    onChangeText={eventName => this.setState({ eventName })} 
                                />

                                <Text style={styles.headerText}>Total Balance </Text>

                                <TextInput 
                                    style={styles.totalBalanceStyle}
                                    placeholder="0.00"
                                    onChangeText={totalBalance => this.setState({ totalBalance })} 
                                    keyboardType='numeric'
                                    maxLength={10}
                                />
                            
                            </View>
                    </View>

                    <View style={{flex: 2}}> 
                        <Text style={styles.headerText}>Expense Information</Text>
                        <View style={styles.box}>
                            <TouchableOpacity underlayColor="white"
                                                onPress = {() => this.props.navigation.navigate('PaidBy', {selectedMembers: this.state.selectedMembers,
                                                    paidBy: this.state.paidBy,
                                                    onSetPaidBy: this.onSetPaidBy})}
                            > 
                                <View style={{paddingLeft: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                                
                                    <Text style={{fontSize: 17, fontWeight: '300'}}>Paid by</Text>

                                    <View style={{paddingRight: 15}}>
                                        <Text style={{fontSize: 17, fontWeight: '500', color:'#A076E8'}}>{this.state.paidBy}  ></Text>
                                    </View>
                                </View>

                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity underlayColor="white"
                                                onPress = {() => this._onChangeSplitBy()}
                            > 
                                <View style={{paddingLeft: 15, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{fontSize: 17, fontWeight: '300'}}>Split</Text>

                                    <View style={{paddingRight: 15}}>
                                        <Text style={{fontSize: 17, fontWeight: '500', color:'#A076E8'}}>{this.state.splitOption} ></Text>
                                    </View>
                                </View>

                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            {/* <TouchableOpacity underlayColor="white"
                                                onPress = {() => this.props.navigation.goBack()}
                            >  */}
                                <View style={{paddingLeft: 15, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{fontSize: 17, fontWeight: '300'}}>{`${this.state.selectedMembers.length} members`}</Text>
                                    {/* <View style={{paddingRight: 15}}>
                                        <Text style={{fontSize: 17, fontWeight: '500', color:'#A076E8'}}>  ></Text>
                                    </View> */}
                                </View>
                            {/* </TouchableOpacity> */}
                        </View>

                        {/* <View style={{padding: 15}}>
                            <View style={{backgroundColor: '#ee6369'}}>
                                <Button
                                    title = "Cancle" 
                                    onPress = {() => params.handlePress()}
                                    color="white"
                                />
                            </View>
                        </View> */}
                    </View>
                </View>
            </DismissKeyboard>
        
        )
    }
}

const styles = StyleSheet.create({
    eventNameStyle: {
        borderWidth: 1,
        borderColor: '#707070',
        height: 40,
        width: '90%',
        paddingLeft: 20,
        backgroundColor: 'white',
        fontSize: 15,
        fontWeight: '200'
    },
    totalBalanceStyle: {
        borderWidth: 1,
        borderColor: '#707070',
        height: 55,
        width: '90%',
        paddingLeft: 20,
        backgroundColor: 'white',
        fontSize: 35,
        fontWeight: '200'
    },
    headerText: {
        fontWeight: '900',
        paddingVertical: 10,
        paddingLeft: 5
    },
    box: {
        paddingBottom: 13,
        paddingTop: 10,
        backgroundColor: 'white',
        borderBottomWidth: 0.5,
        borderTopWidth: 1,
        borderColor:'#EAE7E7'
    },

    box2: {
        paddingBottom: 13,
        backgroundColor: 'white',
        borderBottomWidth: 0.5,
        borderColor:'#EAE7E7',
        paddingTop: 10,
    }

});
