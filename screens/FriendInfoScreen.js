import React from 'react';
import { View, Text ,StyleSheet, AsyncStorage, ScrollView, TextInput, TouchableOpacity, Alert} from "react-native";
import API from "../constants/API";
import GroupsInCommon from '../components/GroupsInCommon';
import Dialog, { DialogContent, DialogFooter, DialogButton } from 'react-native-popup-dialog';


console.disableYellowBox = true;

export default class FriendInfoScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            authKey: "",
            paidBy: 0.00,
            debt: 0.00,
            finalBalance: 0.00,
            isComplete: false,
            visible: false,
            settleAmount: 0.00,
            settleLeft: 0.00,
            whoami: ""
        }
    }

    componentDidMount () {
        this.getBalanceSummaryOfFriends()
        this.getWhoAmI()
    }

    static navigationOptions = ({navigation}) => {
        const { params } = navigation.state;    
        return {
            title: params.name,
            headerStyle: {
                backgroundColor: '#A076E8',
            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
        }
    };

    getWhoAmI = async() => {
        const value = await AsyncStorage.getItem('API_KEY');

        var headers = {
            'API_KEY': value
        }

        API.get(`/whoami`, {headers: headers})
        .then((response) => {
            this.setState({whoami: response.data})
        })
        .catch((error) => {
            console.log(error)
        })

    }
    getBalanceSummaryOfFriends = async() => {
        const { name } = this.props.navigation.state.params;
        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})

        var headers = {
            'API_KEY': this.state.authKey
        }

        API.get(`/get-balance-summary-of-friend?friendName=${name}`, {headers: headers})
        .then((response) => {
            let data = response.data
            this.setState({ paidBy: data["paidBy"], debt: data["debt"],
                            finalBalance: data["finalBalance"], isComplete: true })
        })
        .catch((error) => {
            console.log(error)
        })
    }

    settleLeftAmount = (amount) => {
        const { finalBalance } = this.state

        if (amount == 0) {
            this.setState({ settleAmount: 0.00, settleLeft: 0.00})
        }
        else {
            let total = (Math.abs(finalBalance) - amount).toFixed(2)
            this.setState({settleAmount: amount, settleLeft: total})
        }
    }
    

    onSettleUp = () => {
        const { name } = this.props.navigation.state.params;
        const {settleAmount, finalBalance, settleLeft, whoami, authKey} = this.state

        this.setState({visible: false})

        if (settleAmount > Math.abs(finalBalance)) {
            Alert.alert("Balance error")
        }
        else {
            const payload = {
                totalBalance: settleAmount,
                friendName: name
            };
            let headers = {
                'API_KEY': authKey
            }    

            API.post(`/settle-up`, payload, {headers: headers})
            .then((response) => {
                console.log("OK!")
                this.getBalanceSummaryOfFriends()
            
            })
            .catch((error) => {
                console.log(error)
            })
        }
    }
    
    render () {
        
        const { name } = this.props.navigation.state.params;
        const { finalBalance , settleLeft, whoami} = this.state

        // console.log("whoami", whoami)

        return (
            <ScrollView style={{backgroundColor: '#F3F2FB'}}>
                <View style={{alignItems: 'center'}}>
                    
                </View>
                <GroupsInCommon navigation={this.props.navigation} friendName={name}  />
                <Text style={styles.headerText}>EXPENSE SUMMARY</Text>
                <View style={{padding: 20}} >
                    { finalBalance != 0 ? 
                        
                        <View style={styles.containerBox} >
                            { finalBalance > 0 ? 
                                
                                <View>
                                    <Text style={styles.oweTextStyle}>{name} owe you </Text>
                                    <View style={{alignItems: 'center', paddingTop: 15}}><Text style={styles.balanceTextPositive}>THB {finalBalance} </Text></View>
                                </View>
                                :
                                <View >
                                    <Text style={styles.oweTextStyle}>You owe {name}</Text> 
                                    <View style={{alignItems: 'center', paddingTop: 18}}><Text style={styles.balanceTextNegative}>THB { Math.abs(finalBalance) } </Text></View>
                                    <View style={{paddingTop: 15}}>
                                        <TouchableOpacity
                                            style={styles.buttonBox}
                                            onPress={() => {this.setState({ visible: true })}}
                                        >
                                            <View style={{ alignItems:'center'}}><Text style={{paddingTop: 9 ,fontSize: 15, color:'white', fontWeight:'200'}}>SETTLE UP</Text></View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            
                            }
                        </View>
                        :
                        <Text style={styles.oweTextStyle}> You and {name} owe nothing</Text>
                    
                    }
                </View>
                
                <Dialog visible={this.state.visible}
                        rounded
                        width={0.9}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                text="CANCEL"
                                bordered
                                textStyle={{color:'#ee6369'}}
                                
                                onPress={() => {
                                    this.setState({ visible: false });
                                }}
                                key="button-1"
                                />
                                <DialogButton
                                text="SETTLE UP"
                                bordered
                                textStyle={{color:'#53b77f'}}
                                
                                onPress={this.onSettleUp}
                                key="button-2"
                                />
                            </DialogFooter>
                        }

                >

                <DialogContent
                    style={{
                    backgroundColor: '#F7F7F8',
                    }}
                >
                    <View style={styles.textInputContainer}>
                        <TextInput
                            style={{ fontSize: 30, height: 50, width:140, textAlign: 'center', fontWeight: '500'}}
                            placeholder="0.00"
                            onChangeText={(settleAmount) => this.settleLeftAmount(settleAmount)}
                            keyboardType={'numeric'}
                            maxLength={7}
                        />

                    </View>

                    <Text style={{textAlign: 'center', fontWeight: '200'}}>THB {settleLeft} of {Math.abs(finalBalance)}</Text>
                </DialogContent>


                </Dialog>
            </ScrollView>


        )
    }
}

const styles = StyleSheet.create({
    headerText: {
        fontWeight: '900',
        padding: 10,
        paddingTop: 15,
    },
    containerBox: {
        borderWidth: 1,
        borderColor: '#a076e8',
        borderRadius: 10,
        padding: 20,

        backgroundColor: 'white',
        alignItems: 'center'
    },
    oweTextStyle: {
        fontSize: 20,
        fontWeight: '200'
    },

    balanceTextPositive: {
        fontSize: 25,
        fontWeight: '700',
        color: '#53b77f'
    },
    balanceTextNegative: {
        fontSize: 25,
        fontWeight: '700',
        color: '#ee6369'
    },
    buttonBox: {
        borderWidth: 1,
        borderColor: '#a076e8',
        borderRadius: 10,
        backgroundColor: '#a076e8',
        height: 40
    },

    textInputContainer: {
        alignItems: 'center',
        padding: 10,
    }
    
});
