import React from 'react';
import { View, StyleSheet,  ScrollView} from "react-native";
import CustomMultiPicker from "react-native-multiple-select-list";


export default class PaidByScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }


    render () {
        const { selectedMembers, onSetPaidBy } = this.props.navigation.state.params;

        return (
            <ScrollView style={{backgroundColor: '#F3F2FB', flex:1}}>
                <View>
                    <CustomMultiPicker
                            options={selectedMembers}
                            multiple={false} 
                            returnValue={"label"} // label or value
                            callback={(res)=>{ onSetPaidBy(res) }} // callback, array of selected items
                            rowBackgroundColor={"#fff"}
                            rowHeight={50}
                            rowRadius={5}
                            iconColor={"#a076e8"}
                            iconSize={30}
                            selectedIconName={"ios-checkmark-circle-outline"}
                            unselectedIconName={"ios-radio-button-off"}
                            selected={this.state.selectedMembers} // list of options which are selected by default
                        />
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({

});
