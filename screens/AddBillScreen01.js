import React from 'react';
import { View,StyleSheet, ScrollView, AsyncStorage} from "react-native";
import API from "../constants/API";
import FlatListComp from '../components/FlatListComp';
import Loading from '../components/Loading';
import { NavigationEvents } from 'react-navigation';

export default class AddBillScreen01 extends React.Component {

    constructor(props) {
        super()
        this.state = {
            isLoadingGroup: true,
            groups: [],
            authKey: ""
        }
    }

    componentDidMount() {
        this._retrieveData()
        this.getGroup();
    }

    getGroup = async() => {

        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})

        var headers = {
            'API_KEY': this.state.authKey
        }
        API.get(`/get-group`, {headers: headers})
        .then((response) => {
            this.setState({groups: response.data.sort(), isLoadingGroup: false})
        })
        .catch((error) => {
            console.log(error)
        })
    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('API_KEY');
            if (value == null) {
                this.props.navigation.navigate('Auth')               
            }
        } catch (error) {
            console.log(error)
        // Error retrieving data
        }
    };



    handleClickFriend = (groupName) => {
        this.props.navigation.navigate('AddBill02', {groupName : groupName, authKey: this.state.authKey });
    }

    render () {

        const { isLoadingGroup } = this.state

        if (isLoadingGroup) {
            return <Loading />
        }
        else {
            return (
            <ScrollView style={{backgroundColor: '#F3F2FB'}}>
                <View >                    
                    <FlatListComp 
                        data={this.state.groups}
                        header={"GROUPS"}
                        handleClick={this.handleClickFriend}
                    />
                </View>

                <NavigationEvents onDidFocus={() => this.getGroup()} />
            </ScrollView>
        )
        }
    }
}

const styles = StyleSheet.create({

});
