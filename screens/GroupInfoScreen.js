import React from 'react';
import { View, Text ,StyleSheet,FlatList, ScrollView, AsyncStorage, TouchableOpacity, Alert} from "react-native";
import API from "../constants/API";
import SegmentedControlTab from "react-native-segmented-control-tab";
import Events from '../components/Events';
import moment from 'moment'
import { Avatar } from "react-native-elements";
import Icon from 'react-native-vector-icons/Ionicons';
import ReactNativePickerModule from 'react-native-picker-module'
import _ from 'lodash'

export default class GroupInfoScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            authKey: "",
            selectedIndex: 0,
            eventsList: [],
            friendList: [],
            friendToChoose: [],
            selectedValue: null,

        }
    }

    componentDidMount () {
        this.getEventsInGroup()
        this.getMemberInGroup()
    }


    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;    
        return {
            title: params.name,
            headerStyle: {
                backgroundColor: '#A076E8',
            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
        }
    };


    handleIndexChange = index => {
        this.setState({
            selectedIndex: index
        });
    };

    getMemberInGroup = async() => {
        const { name } = this.props.navigation.state.params;
        const auth = await AsyncStorage.getItem('API_KEY');

        var headers = {
            'API_KEY': auth
        }
        
        API.get(`/get-friend-from-groupName?groupName=${name}`, {headers: headers})
        .then((response) => {
            console.log("OK!")
            this.setState({ friendList: response.data })
            // console.log(response.data)

        })
        .catch((error) => {
            console.log(error)
        })
    }

    getEventsInGroup = async() => {
        const { name } = this.props.navigation.state.params;
        const auth = await AsyncStorage.getItem('API_KEY');

        var headers = {
            'API_KEY': auth
        }

        API.get(`/get-events-in-group?groupName=${name}`, {headers: headers})
        .then((response) => {
            console.log("OK!")
            let data = response.data

            data  = (_.sortBy(data, 'date')).reverse()
            this.setState({ eventsList: data})
        })
        .catch((error) => {
            console.log(error)
        })
    }

    _keyExtractor = (item) => item.eventUUID.toString();

    _renderItem = ({item}) => {
        let date = moment(item.date)
        date = moment().format('ll'); 
        return (<View style={{paddingBottom: 20}}><Events eventName={item.eventName} createdBy={item.createdBy} 
        date={date} totalBalance={item.totalBalance} members={item.members}/></View>)
    }
        
    _keyExtractorGroup = (item) => item;

    _renderItemGroup = ({item}) => {
        const { handleClickFriend } = this.props.navigation.state.params;

        return (
            
            <View style={{paddingBottom: 10}} >
                <TouchableOpacity onPress={() => handleClickFriend(item)}>
                    <View style={styles.friendList}>
                        <View style={{flexDirection: 'row'}}>
                            
                                <View style={{paddingLeft: 5, paddingTop: 6}}>
            
                                    <Avatar
                                        size="small"
                                        rounded
                                        title={item.substring(0,2)}
                                        activeOpacity={0.7}
                                        overlayContainerStyle={{backgroundColor: '#a076e8'}}
                                    />
                                </View>
                                {/* onPress={() => this.props.handleClick(item.name)} */}
                                <View>
                                    
                                    
                                        <Text style={{fontSize: 15, fontWeight: '200', paddingLeft: 5,  paddingTop: 14}}> {item} </Text>
                                    
                                </View>
                                
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    handleAddFriendInGroup = async() => {
        
        const {friendList} = this.state

        const auth = await AsyncStorage.getItem('API_KEY');

        var headers = {
            'API_KEY': auth
        }

        const payload = {
            member: friendList
        };

        API.post(`/get-friend-left`, payload,{headers: headers})
        .then((response) => {
            console.log("OK!")
            if (response.data.length == 0) {
                Alert.alert("All of your friends already in the group")
                

            }
            else {
                this.setState({friendToChoose: response.data})
                this.pickerRef.show()
            }

        })
        .catch((error) => {
            console.log(error)
        })
    }

    onAddNewMember = async(member) => {
        const { name } = this.props.navigation.state.params;
        const {friendToChoose} = this.state
        const auth = await AsyncStorage.getItem('API_KEY');
        // console.log(friendToChoose[member])
        // /add-friend-in-group
        var headers = {
            'API_KEY': auth
        }
        const payload = {

        }

        API.post(`/add-friend-in-group?friendName=${friendToChoose[member]}&groupName=${name}`, payload,{headers: headers})
        .then((response) => {
            console.log("OK!")
            this.getMemberInGroup()
            // this.setState({friendToChoose: response.data})
            // this.pickerRef.show()

        })
        .catch((error) => {
            console.log(error)
        })

    }

    

    render () {
        const { eventsList, friendList } = this.state

        return (
            <View style={{backgroundColor: '#F3F2FB', flex: 1}}>                
                <View style={{ padding: 25 }}>
                    <SegmentedControlTab
                        values={["Events", "Members"]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                        activeTabStyle={styles.activeTabStyle}
                        tabStyle={styles.tabStyle}
                        tabTextStyle={styles.tabTextStyle}
                    />

                    
                
                    <ScrollView >
                    {this.state.selectedIndex == 0 ?
                    
                    <View style={{paddingTop: 30}}>
                        <FlatList
                            data={eventsList}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem}
                        />
                    </View>
                    :
                    
                    <View style={{paddingTop: 30}}>
                        <View style={{paddingTop: 5, paddingBottom: 10}}>
                            <TouchableOpacity onPress={() => this.handleAddFriendInGroup()}>
                                <View style={styles.addFriendList}>
                                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 7}}>
                                        <Icon name="ios-add-circle-outline" size={30} color='#A076E8'/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <FlatList
                            data={friendList}
                            keyExtractor={this._keyExtractorGroup}
                            renderItem={this._renderItemGroup}
                        />
                    </View>
                    }
                    </ScrollView>
                </View>

                <ReactNativePickerModule
                    pickerRef={e => this.pickerRef = e}
                    value={this.state.selectedValue}
                    title={"Select a friend"}
                    items={this.state.friendToChoose}
                    onValueChange={(index) => this.onAddNewMember(index)}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    activeTabStyle: {
        backgroundColor: '#a076e8',
        borderWidth: 1,
        borderColor: '#a076e8',
    },
    tabStyle: {
        borderWidth: 1,
        borderColor: '#a076e8',
        height: 35
        
    },

    tabTextStyle: {
        color: '#a076e8'
    },
    friendList: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#a076e8',
        borderRadius: 10,
        paddingBottom: 10,
        height: 50

    },

    addFriendList: {
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        borderWidth: 2,
        borderColor: '#a076e8',
        borderRadius: 10,
        paddingBottom: 10,
        height: 50,
        borderStyle: 'dashed'

    }

});
