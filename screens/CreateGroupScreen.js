import React from 'react';
import { View, StyleSheet, Button, Alert, AsyncStorage, Keyboard, TouchableWithoutFeedback} from "react-native";
import { NavigationActions } from 'react-navigation';
import API from "../constants/API";
import TextInputComp from '../components/TextInputComp';
import Loading from '../components/Loading';
import CustomMultiPicker from "react-native-multiple-select-list";

const backAction = NavigationActions.back({
    key: null
})

const DismissKeyboard = ({children})=>(
    <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
export default class CreateGroupScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            groupName: "",
            members: [],
            authKey: "",
            isLoading: true,
            selectedItems: [],
            selectedMembers: []
        }
    }

    componentDidMount () {
        this.props.navigation.setParams({handleSave: () => this._handleAddGroup()});
        this.getFriends();
    }

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
        return {
            headerRight: (
                <Button
                    title = "Save"
                    onPress = {() => params.handleSave()}
                    color="#fff"
                />
            )
        }
    }

    getAuth = async() => {
        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})
    }

    getFriends = async() => {
        
        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})
        var headers = {
            'API_KEY': this.state.authKey
        }
        API.get(`/get-friends-name`, {headers: headers})
            .then((response) => {
                this.setState({members: response.data, isLoading: false})
            })
            .catch((error) => {
                console.log(error)
            })
    }

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;    
        return {
            title: "Create a group",
            headerStyle: {
                backgroundColor: '#A076E8',
            },
            headerTintColor: '#FCFCFC',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
        }
    };


    _handleAddGroup = async() => {
        let canCreate = true;
        let memberName = [];

        if (this.state.groupName == "") {
            Alert.alert("Please enter group name")
            canCreate =  false;
        }
        if (this.state.selectedItems < 1) {
            Alert.alert("Please select a member")
            canCreate =  false;
        }

        this.state.selectedItems.map((index) => {        
            memberName.push(index)
        })

        if (canCreate) {
            const payload = {
                members: memberName,
            };
            const headers = {
                'API_KEY': this.state.authKey
            }
            API.post(`/create-group?groupName=${this.state.groupName}`, payload, {headers: headers})
            .then((response) => {
                console.log("OK!")
                const { params} = this.props.navigation.state;
                params.getGroup()
                this.props.navigation.dispatch(backAction)
            })
            .catch((error) => {
                
                Alert.alert("The group name has already existed! Please try again")
            })
        }
    }

    render () {
        const isLoading = this.state.isLoading;

        if (isLoading == true) {
            return <Loading />
        }
        else {
            return (
            <DismissKeyboard>
                <View style={{flex:1, backgroundColor: '#F3F2FB'}}>
                    <View style={styles.container}>
                        <TextInputComp 
                            placeholder="Group Name"
                            onChangeText={groupName => this.setState({groupName})}
                        />    
                    </View>

                    <View style={{paddingTop: 20}}>
                        <CustomMultiPicker
                            options={this.state.members}
                            multiple={true} 
                            returnValue={"label"} // label or value
                            callback={(res)=>{ this.setState({selectedItems: res}) }} // callback, array of selected items
                            rowBackgroundColor={"#fff"}
                            rowHeight={50}
                            rowRadius={5}
                            iconColor={"#a076e8"}
                            iconSize={30}
                            selectedIconName={"ios-checkmark-circle-outline"}
                            unselectedIconName={"ios-radio-button-off"}
                            selected={this.state.selectedItems} // list of options which are selected by default
                            itemStyle={styles.textStyle}
                            labelStyle={styles.textStyle}
                        />
                    </View>

                    <View style={{paddingTop: 15}}> 
                        <View style={{backgroundColor: '#53b77f'}}>
                            <Button
                                title = "Save" 
                                onPress = {this._handleAddGroup}
                                color="white"
                            />
                        </View>
                    </View>      
                </View>
            </DismissKeyboard>
        )
        }        
    }
}

const styles = StyleSheet.create({

    container: {
        alignItems: "center", 
        justifyContent: "center",
        paddingTop: 10,
    },
    textStyle: {
        fontSize: 30,
        color: 'red',
        backgroundColor: 'green'
    }

});
