import React from 'react';
import { View, Text,  StyleSheet, AsyncStorage, Image,TouchableOpacity,Alert, ImageBackground , Keyboard, TouchableWithoutFeedback} from "react-native";
import {  StackActions, NavigationActions } from 'react-navigation';
import GradientButton from 'react-native-gradient-buttons/src';
import TextInputComp from "../components/TextInputComp";
import API from "../constants/API";
import Icon from 'react-native-vector-icons/Ionicons';
import splitIcon from '../assets/Icons/split.png'
import bg from '../assets/bg.png'

const DismissKeyboard = ({children})=>(
    <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);


const resetAction = StackActions.reset( {
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'SignUp'})]
})
export default class SignInScreen extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            userName : "",
            password: "",
            loginError: false,

        };
    }

    //DONT forget to handle when there is login error

    render () {
        const navigate = this.props.navigation;
        return (
            

            <ImageBackground source={bg} style={{width: '100%', height: '100%'}}>
                <DismissKeyboard>

                    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>

                        <View style={styles.container}>
                                <Image source={splitIcon} style={{width: 100, height: 80}}/>
                                <View style={{paddingTop: 10}}>
                                    <Icon name="ios-person" size={25} color='#a2a2a2'/>
                                </View>
                                
                                <View style={styles.boxGab}>
                                    <TextInputComp
                                        placeholder="Username"
                                        onChangeText={userName => this.setState({ userName })} 
                                        maxLength={15}
                                        autoCorrect={false}
                                    />
                                </View>

                                <View style={styles.boxGab}>
                                    <TextInputComp
                                        placeholder="Password"
                                        onChangeText={password => this.setState({ password })} 
                                        maxLength={15}
                                        secureTextEntry={true}
                                    />
                                </View>

                                <View style={styles.boxGab}>
                                    <GradientButton
                                        textStyle = { styles.buttonText}
                                        text = "Login"
                                        gradientBegin = '#A076E8'
                                        gradientEnd = '#B1C4F8'
                                        gradientDirection = 'horizontal'
                                        height = {36}
                                        width = {170}
                                        radius = {100}
                                        onPressAction = {this.handleLogin}
                                    />
                                </View>
                        </View>

                        <TouchableOpacity onPress={ () => {navigate.dispatch(resetAction)}} underlayColor="white">
                            <View style={{paddingTop: 15}}>
                                <View style={{alignItems:'center'}} >
                                    <Text style={{fontWeight: '200', fontSize: 11}} >Don't have an account ?</Text> 
                                </View>
                                <View style={{paddingTop: 5, alignItems:'center'}}>
                                    <Text style={{fontWeight: '100', color:'#A076E8'}}>CREATE AN ACCOUNT</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                    
                    </View>
                
                </DismissKeyboard>

            </ImageBackground>

        )
    }

    handleLogin = () => {

        let completeField = true;

        if (this.state.userName.length <= 5) {
            completeField = false;
            this.setState({loginError: true})
        }
        if (this.state.password.length <= 5) {
            completeField =false;
            this.setState({loginError: true})
        }
        if (completeField) {

            const payload = {
                username: this.state.userName,
                password: this.state.password
            };


            //send in body
            API.post(`/user-login`, payload)
            .then((response) => {   
                data=response.data
                // console.log(data["API_KEY"])   
                this._storeData(data["API_KEY"])   
                // console.log("hi") 
                // this.props.navigation.dispatch(resetActionMain)
                this.props.navigation.navigate('Main')
                // console.log("hi2")
            })
            .catch((error) => {
                // console.log(error.data)
                Alert.alert("Wrong username/password")
                
                this.setState({loginError: true})
            })
        }

    }

    _storeData = async (data) => {
        
        try {
            await AsyncStorage.setItem('API_KEY', data);
        } catch (error) {
            console.log(error)
        // Error saving data
        }
        // this._retrieveData()

    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('API_KEY');
            if (value !== null) {
                // We have data!!
                // console.log(">> "+ value);
        }
        } catch (error) {
        // Error retrieving data
        }
    };
}


const styles = StyleSheet.create({
    buttonText: {
        // fontFamily: 'WorkSans-Regular',
        fontSize: 16,
        color: '#fcfcfc',
        fontWeight: '100',
    
    },
    boxGab: {
        paddingTop: 20
    },
    container: {
        alignItems: "center", 
        justifyContent: "center",
        backgroundColor: "white",
        borderColor: 'white',
        borderRadius: 25,
        borderWidth: 1,
        borderStyle: 'solid',
        
        paddingTop: 80,
        paddingBottom: 80,
        paddingLeft: 30,
        paddingRight: 30,

        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.1,
        elevation: 1

    }
    });