import React from 'react';
import { View, Text ,StyleSheet, ScrollView, Button, Alert} from "react-native";
import { StackActions, NavigationActions } from 'react-navigation';
import API from "../constants/API";
import CustomMultiPicker from "react-native-multiple-select-list";
import Icon from 'react-native-vector-icons/Ionicons';

export default class AddBillScreen02 extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            members: [],
            authKey: "",
            selectedMembers: []

        }
    }



    static navigationOptions = ({navigation}) => {

        const {params} = navigation.state;
    
        return {
            headerRight: (
                <Button
                    title = "Done"
                    onPress = {() => params.handlePress()}
                    color="#fff"
                />
            )
        }
    }
    
    componentDidMount () {
        
        this.props.navigation.setParams({handlePress: () => this._onPressButton()});

        const groupName = this.props.navigation.state.params.groupName;
        const authKey = this.props.navigation.state.params.authKey;
        this.setState({authKey: authKey})


        var headers = {
            'API_KEY': authKey
        }
        
        API.get(`/get-friend-from-groupName?groupName=${groupName}`, {headers: headers})
        .then( (response) => {
            this.setState({members: response.data.sort()})
            
        })
        .catch( (error) => {
            console.log(error)
        })

    }

    

    _onPressButton = () => {
        const {groupName, authKey}= this.props.navigation.state.params

        if (this.state.selectedMembers.length < 2) {
            Alert.alert("Please select at least 2 members")
        }
        else {
            this.props.navigation.navigate('AddBillMain', 
                                        {selectedMembers: this.state.selectedMembers,
                                            groupName: groupName, 
                                            authKey: authKey});

        }                                          
    }



    render () {
        return (
            <ScrollView style={{backgroundColor: '#F3F2FB', flex:1}}>
                <View style={{paddingTop: 20}}>
                    <CustomMultiPicker
                        options={this.state.members}
                        multiple={true} 
                        returnValue={"label"} // label or value
                        callback={(res)=>{ this.setState({selectedMembers: res}) }} // callback, array of selected items
                        rowBackgroundColor={"#fff"}
                        rowHeight={50}
                        rowRadius={5}
                        iconColor={"#a076e8"}
                        iconSize={30}
                        selectedIconName={"ios-checkmark-circle-outline"}
                        unselectedIconName={"ios-radio-button-off"}
                        selected={this.state.selectedMembers} // list of options which are selected by default
                        itemStyle={styles.textStyle}
                        labelStyle={styles.textStyle}
                    />
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 30,
        color: 'red',
        backgroundColor: 'green'
    }

});
