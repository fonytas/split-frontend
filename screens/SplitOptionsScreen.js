import React from 'react';
import {  StyleSheet, Dimensions, Button, Alert } from 'react-native';
import { TabView,TabBar } from 'react-native-tab-view';
import SplitEqually from '../components/SplitEqually';
import SplitUnequally from '../components/SplitUnequally';

export default class SplitOptionsScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            index: this.props.navigation.state.params.index,
            routes: [
                { key: 'first', title: 'Split Equally' },
                { key: 'second', title: 'Split by exact amount' },
            ],
            isDone: false
            
        }
    }

    componentDidMount () {
        this.props.navigation.setParams({handleGoBack: () => this._handleGoBack()});
    }
    

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
    
        return {
            headerLeft: (
                <Button
                    title = "Cancle"
                    onPress = {() => navigation.goBack()}
                    color="#fff"
                />
            ),

            headerRight: (
                <Button
                    title = "Done"
                    onPress = {() => params.handleGoBack()}
                    color="#fff"
                />
            )
        }
    }


    onIsDone = (bal) => {
        if (bal == 0) {
            this.setState({isDone: true})
        } 
    }

    _handleGoBack = () => {
        
        const {isDone} = this.state
        if (isDone || this.state.index == 0) {
            this.props.navigation.goBack()
            
        }
        else{
            Alert.alert("Total balance is not fulfilled")
        }
        
    }  
    _handleIndexChange = index => {
        this.setState({ index });
        this.props.navigation.state.params.handleIndexChange(index)
    }


    render() {
        const {totalBalance, selectedMembers, summaryBalance,  remainingAmount,splitUnquallySummary, setSplitUnquallySummary} = this.props.navigation.state.params;
        return (
            <TabView
                navigationState={this.state}
                renderScene={({route}) => {
                        switch (route.key) {
                        case 'first':
                            return <SplitEqually totalBalance={totalBalance} 
                                                selectedMembers={selectedMembers}
                                                summaryBalance={summaryBalance}
                                    />
                        case 'second':
                            return <SplitUnequally totalBalance={totalBalance}
                                                    selectedMembers={selectedMembers}
                                                    remainingAmount={remainingAmount}
                                                    splitUnquallySummary={splitUnquallySummary}
                                                    setSplitUnquallySummary={setSplitUnquallySummary} 
                                                    onIsDone={this.onIsDone} />
                        default:
                            return null;
                    }

                }}
                
                onIndexChange={this._handleIndexChange}
                initialLayout={{ width: Dimensions.get('window').width }}
                renderTabBar={props =>
                    <TabBar
                        {...props}
                        indicatorStyle={{
                            backgroundColor: 'white',
                        }}
                        inactiveColor='#ACA1BF'
                        style={{
                            backgroundColor: '#A076e8',
                            
                            paddingTop: 10
                        }}
                    />
                }
            />
    );
}
}
    
const styles = StyleSheet.create({
    scene: {
        flex: 1,
},
});