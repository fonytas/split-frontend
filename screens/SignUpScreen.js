import React from 'react';
import { View, Text , StyleSheet, Image, TouchableOpacity, ImageBackground, Keyboard, TouchableWithoutFeedback} from "react-native";
import TextInputComp from "../components/TextInputComp";
import { StackActions, NavigationActions } from 'react-navigation';
import GradientButton from 'react-native-gradient-buttons/src';
import API from "../constants/API";
import Icon from 'react-native-vector-icons/Ionicons';
import splitIcon from '../assets/Icons/split.png'
import bg from '../assets/bg.png'

const DismissKeyboard = ({children})=>(
    <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

const resetAction = StackActions.reset( {
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'SignIn'})]
})
export default class SignUpScreen extends React.Component {


    constructor (props) {
        super(props);
        this.state = {
            userName : "",
            password: "",
            confirmPassword: "",
            userNameError: false,
            passwordError: false,
            loginError: false,
            passwordMismatch: false

        };
    }


    render () {
        const navigate = this.props.navigation
        return (
            <ImageBackground source={bg} style={{width: '100%', height: '100%'}}>

                <DismissKeyboard>

                    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>

                        <View style={styles.container}>

                            <Image source={splitIcon} style={{width: 100, height: 80}}/>
                            <Icon name="ios-person-add" size={25} color='#a2a2a2'/>

                            <View style={styles.boxGab}>
                                <TextInputComp
                                    placeholder="Username"
                                    onChangeText={userName => this.setState({ userName })} 
                                    maxLength={15}
                                    autoCorrect={false}
                                />
                            </View>


                            <View style={styles.boxGab}>
                                <TextInputComp
                                    placeholder="Password"
                                    onChangeText={password => this.setState({ password })} 
                                    maxLength={20}
                                    secureTextEntry={true}
                                />
                            </View>


                            <View style={styles.boxGab}>
                                <TextInputComp
                                    placeholder="Confirm Password"
                                    onChangeText={confirmPassword => this.setState({ confirmPassword })} 
                                    maxLength={15}
                                    secureTextEntry={true}
                                />
                            </View>
                            <View style={styles.boxGab}>
                                <GradientButton
                                    textStyle = { styles.buttonText}

                                    text = "Sign Up"
                                    gradientBegin = '#A076E8'
                                    gradientEnd = '#B1C4F8'
                                    gradientDirection = 'horizontal'
                                    height = {36}
                                    width = {170}
                                    radius = {100}
                                    onPressAction = {this.handleSignUp }
                                />
                            </View>
                            <View style= { styles.gap }>


                                { this.state.userNameError && <Text style= {{color: 'red', textAlign: 'center'}}> * Username less than 5 characters</Text>}
                                { this.state.passwordError && <Text style= {{color: 'red', textAlign: 'center'}}> ** Password less than 5 characters</Text>}
                                { this.state.passwordMismatch && <Text style= {{color: 'red', textAlign: 'center'}}> *** Password mismatch </Text>}
                                { this.state.loginError && <Text style= {{color: 'red', textAlign: 'center'}}> ***** Invalid username </Text>}
                            </View>

                        </View>

                        <TouchableOpacity onPress={ () => {navigate.dispatch(resetAction)}} underlayColor="white">
                            <View style={{paddingTop: 15}}>
                                <View style={{alignItems:'center'}} >
                                    <Text style={{fontWeight: '200', fontSize: 11}} >Already have an account?</Text> 
                                </View>
                                <View style={{paddingTop: 5, alignItems:'center'}}>
                                    <Text style={{fontWeight: '100', color:'#A076E8'}}>LOG IN</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        
                        
                    </View>
                </DismissKeyboard>
            </ImageBackground>
        )
    }

    clearState = () => {
        this.setState({userNameError: false, passwordMismatch: false, passwordError: false, loginError: false})
    }
    handleSignUp = () => {
        this.clearState()

        let completeField = true;
        if (this.state.userName.length <= 5) {
            this.setState({userNameError: true})
            completeField = false;
        }
        if (this.state.password != this.state.confirmPassword) {
            this.setState({passwordMismatch: true})
            completeField = false;
        }
        if (this.state.password <= 5) {
            this.setState({passwordError: true})
            completeField = false;
        }
        if (completeField) {

            const payload = {
                username: this.state.userName,
                password: this.state.password
            };

            API.post(`/user-register`, payload)
            .then((response) => {                
                this.props.navigation.dispatch(resetAction)
            })
            .catch((error) => {
                this.setState({loginError: true})
            })
        }


    }
}

const styles = StyleSheet.create({
    buttonText: {
        fontSize: 16,
        color: '#fcfcfc',
        fontWeight: '100',
    
    },
    boxGab: {
        paddingTop: 20
    },

    gap: {
        paddingTop: 10

    },

    container: {
        alignItems: "center", 
        justifyContent: "center",
        backgroundColor: "white",
        borderColor: 'white',
        borderRadius: 25,
        borderWidth: 1,
        borderStyle: 'solid',
        
        paddingTop: 30,
        paddingBottom: 80,
        paddingLeft: 30,
        paddingRight: 30,

        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.1,
        elevation: 1

    }
    // button: {
    //     backgroundImage: 
    // }

    });
    