import React from 'react';
import { View, Text ,StyleSheet, Button, AsyncStorage, FlatList} from "react-native";
import { StackActions, NavigationActions, NavigationEvents } from 'react-navigation';
import API from "../constants/API";
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment'
import _ from 'lodash'

const resetAction = StackActions.reset( {
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Auth'})],
    key: null
})
export default class ProfileScreen extends React.Component {

    constructor(props) {
        super()
        this.state = {
            authKey: "",
            whoami: "",
            settleUpHistory: []
            

        }
    }

    componentDidMount () {
        this.getWhoAmI()
        this.getSettleUpHistory()


    }

    getWhoAmI = async() => {


        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})

        var headers = {
            'API_KEY': value
        }

        API.get(`/whoami`, {headers: headers})
        .then((response) => {
            // console.log(response.data)

            this.setState({whoami: response.data})
            

        })
        .catch((error) => {
            console.log(error)
        })

    }

    getSettleUpHistory = async() => {
        console.log("getSettleUpHistory")

        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})

        var headers = {
            'API_KEY': value
        }

        API.get(`/get-settle-up-history`, {headers: headers})
        .then((response) => {
            // console.log(response.data)
            let data = response.data

            
            data  = (_.sortBy(data, 'Date')).reverse()
            this.setState({settleUpHistory: data})

            

        })
        .catch((error) => {
            console.log(error)
        })


    }


    _clearAsyncStorage = async() => {
        AsyncStorage.clear();
        this.props.navigation.dispatch(resetAction)
    }

    keyExtractor = (item, index) => item.Date;

    // _renderItem = ({item}) => {
    //     let date = moment(item.date)
    //     date = moment().format('ll'); 
    //     return (<View style={{paddingBottom: 20}}><Events eventName={item.eventName} createdBy={item.createdBy} 
    //     date={date} totalBalance={item.totalBalance} members={item.members}/></View>)
    // }

    
    _renderItem = ({item}) => {
        const {whoami} = this.state
        
        let date = moment(item.Date)
        date = moment().format('ll'); 

        // console.log(whoami)
        let receiver = item.Receiver
        let giver = item.Giver
        let icon = <Icon name="ios-arrow-round-forward" size={20} color='#53B77F'/>
        

        console.log(receiver)
        if (item.Receiver == whoami ) {
            receiver = "You"
        }
        if (item.Giver == whoami) {
            giver = "You"
        }
        if (giver == "You") {
            icon =  <Icon name="ios-arrow-round-back" size={20} color='#EE6369'/>
        }

        
        return(
            <View style={{paddingTop: 15, paddingLeft: 10}}> 

                <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>

                    <View style={{flexDirection: 'row'}}>
                        <View style={{paddingRight: 5}}>
                            {icon}
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontSize: 15, fontWeight: '200'}}>{giver}  gives </Text>
                            <Text style={{fontSize: 15, fontWeight: '600'}}> THB{item.Amount} </Text>
                            <Text style={{fontSize: 15, fontWeight: '200'}}> to  {receiver} </Text>
                        </View>



                    </View>

                    <View style={{paddingRight: 5}}>
                        <Text style={{fontSize: 13, fontWeight: '200', color: '#a2a2a2'}}> {date} </Text>
                    </View>
                </View>

            </View>
        )
    }

    // let date = moment(item.date)
    //     date = moment().format('ll'); 

    render () {
        const {whoami, settleUpHistory} = this.state


        return (
            <View style={{backgroundColor: '#F3F2FB', flex: 1, justifyContent:'space-between'}}>

                <View>
                    <View style={{backgroundColor: 'white'}}>

                        <View style={{alignItems: 'center', paddingTop: 10}}>
                            <Icon name="ios-contact" size={100} color='#A076E8'/>
                        </View>

                        <View style={{alignItems: 'center', paddingBottom: 20}}>
                            <View style={{flexDirection: 'row'}}> 
                                <Text style={{fontSize: 15, fontWeight: '600'}}>Username: </Text>

                                <Text style={{fontSize: 15, fontWeight: '200', color:'#A076E8'}}> {whoami} </Text>
                            </View>

                        </View>

                        
                    </View>

                    <View>
                        <FlatList
                            data={settleUpHistory}
                            extraData={this.state}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem}
                        />
                    
                    
                    
                    </View>
                </View>

                <View style={{padding: 20}}>
                    <View style={{backgroundColor: '#EE6369'}}>
                        <Button
                            onPress={this._clearAsyncStorage}
                            title="LOG OUT"
                            color="white"
                        />
                    </View>
                </View>

                <NavigationEvents onDidFocus={() => this.getSettleUpHistory()} />

            </View>


        )
    }

    
}

const styles = StyleSheet.create({

});
