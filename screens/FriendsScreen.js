import React from "react";
import { View, StyleSheet, ScrollView,  AsyncStorage, RefreshControl} from "react-native";
import { NavigationEvents } from 'react-navigation';
import API from "../constants/API";
import TotalBalance from "../components/TotalBalance";
import GroupList from "../components/GroupList";
import FlatListComp from "../components/FlatListComp"
import Loading from "../components/Loading"


export default class FriendsScreen extends React.Component {

    constructor(props) {
        super()
        this.state = {
            names: [],
            isLoadingFriend: true,
            isLoadingGroup: true,
            groups: [],
            youOwe: 0,
            youAreOwed: 0,
            totalBalance: 0,
            hasDebt: false,
            refreshing: false,
        }
    }

    
    componentDidMount () {
        // console.log("whta")
        this.getInfo()
    }

    getInfo = () => {
        this._retrieveData()
        this.getFriends()
        this.getGroup()
        this.getBalanceSummary()

    }

    _onRefresh = () => {

        this.setState({refreshing: true});
        this.getInfo()
        this.setState({refreshing: false})
        // fetchData().then(() => {
        //     this.setState({refreshing: false});
        // });
    }


    getGroup = async() => {
        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})

        var headers = {
            'API_KEY': this.state.authKey
        }

        API.get(`/get-group`, {headers: headers})
        .then((response) => {
            // console.log(response.data)
            // console.log(response.data)
            // console.log("Sorted: ",response.data.sort())
            this.setState({groups: response.data.sort(), isLoadingGroup: false})

        })
        .catch((error) => {
            console.log(error)
        })
    }

    getFriends = async () => {
        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})

        var headers = {
            'API_KEY': this.state.authKey
        }

        API.get(`/get-friends`, {headers: headers})
            .then((response) => {
                // console.log(response.data.sort())
                // console.log("sorted", response.data.sort())
                this.setState({names: response.data.sort(), isLoadingFriend:false})
            })
            .catch((error) => {
                console.log(error)
            })
    }

    handleClickFriend = (friendName) => {
        this.props.navigation.navigate('FriendInfo', {name : friendName });
    }

    getBalanceSummary = async() => {

        const value = await AsyncStorage.getItem('API_KEY');
        this.setState({authKey: value})
        var headers = {
            'API_KEY': this.state.authKey
        }
        API.get(`/get-your-balance-summary`, {headers: headers})
        .then((response) => {
            this.setState({youOwe: response.data["youOwe"], youAreOwed: response.data["youAreOwed"]})
            this.handleTotalBalance()
        })
        .catch((error) => {
            console.log(error)
        })
        
    }

    handleTotalBalance = () => {
        this.setState({totalBalance: (this.state.youAreOwed - this.state.youOwe).toFixed(2) })
        if (this.state.totalBalance < 0) {
            this.setState({hasDebt: true})
        }
    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('API_KEY');
            if (value == null) {
                this.props.navigation.navigate('Auth')
            }
            
        } catch (error) {
            console.log(error)
        // Error retrieving data
        }
    };

    handleClickGroup = (groupName) => {
        this.props.navigation.navigate('GroupInfo', {name : groupName, handleClickFriend: this.handleClickFriend });
    }

    render () {
        const {isLoadingFriend, isLoadingGroup, totalBalance, youAreOwed, youOwe, hasDebt} = this.state
        // if (isLoadingFriend || isLoadingGroup) {
        //     return <Loading />
        // }
        // else {
        return (

            <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#F3F2FB'}}>
                <View style={styles.TotalBalanceTab} >
                    <TotalBalance totalBalance={totalBalance} youAreOwed={youAreOwed} youOwe={youOwe} hasDebt={hasDebt}  />
                </View>

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>
                    <GroupList  navigation={this.props.navigation} 
                                groups={this.state.groups} getGroup={this.getGroup}
                                handleClickGroup={this.handleClickGroup}
                                handleClickFriend={this.handleClickFriend}
                                
                        />
                    {/* <FriendList navigation={this.props.navigation} names={this.state.names} getFriends={this.getFriends}/> */}
                    <FlatListComp data={this.state.names} header={"FRIENDS"} getFriends={this.getFriends} 
                                handleClick={this.handleClickFriend}/>                      
                </ScrollView>
                <NavigationEvents onDidFocus={() => this.getInfo()} />
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    TotalBalanceTab: {
        height: 108,
        width: '100%',
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#E9EEF2'
    }

});
